﻿using System;
using NitroxModel.Helper;
using UnityEngine;

namespace NitroxClient.Unity.Helper
{
    public static class GameObjectHelper
    {
        public static T RequireComponent<T>(this GameObject o) where T : class
        {
            T component = o.GetComponent<T>();
            Validate.NotNull(component, o.name + " 没有类型的组件 " + typeof(T));

            return component;
        }

        public static T RequireComponentInChildren<T>(this GameObject o, bool includeInactive = false) where T : class
        {
            T component = o.GetComponentInChildren<T>(includeInactive);
            Validate.NotNull(component, o.name + " 没有组件类型 " + typeof(T) + " 在子组件");

            return component;
        }

        public static T RequireComponentInParent<T>(this GameObject o) where T : class
        {
            T component = o.GetComponentInParent<T>();
            Validate.NotNull(component, o.name + " 没有组件类型 " + typeof(T) + " 在父组件");

            return component;
        }

        public static Transform RequireTransform(this Transform tf, string name)
        {
            Transform child = tf.Find(name);

            if (child == null)
            {
                throw new ArgumentNullException(tf + " 不包含 \"" + name + "\"");
            }

            return child;
        }
        public static Transform RequireTransform(this GameObject go, string name) => go.transform.RequireTransform(name);
        public static Transform RequireTransform(this MonoBehaviour mb, string name) => mb.transform.RequireTransform(name);

        public static GameObject RequireGameObject(this Transform tf, string name) => tf.RequireTransform(name).gameObject;
        public static GameObject RequireGameObject(this GameObject go, string name) => go.transform.RequireGameObject(name);
        public static GameObject RequireGameObject(this MonoBehaviour mb, string name) => mb.transform.RequireGameObject(name);

        public static GameObject RequireGameObject(string name)
        {
            GameObject go = GameObject.Find(name);
            Validate.NotNull(go, "找不到具有的全局游戏对象 " + name + "!");

            return go;
        }
    }
}
