﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NitroxClient.Communication.Abstract;
using NitroxClient.Communication.Packets.Processors.Abstract;
using NitroxClient.GameLogic;
using NitroxClient.GameLogic.Helper;
using NitroxModel.DataStructures.Util;
using NitroxModel.Logger;
using NitroxModel_Subnautica.Packets;
using UnityEngine;

namespace NitroxClient.Communication.Packets.Processors
{
    public class ExosuitArmActionProcessor : ClientPacketProcessor<ExosuitArmActionPacket>
    {
        private readonly IPacketSender packetSender;
        private readonly ExosuitModuleEvent exosuitModuleEvent;

        public ExosuitArmActionProcessor(IPacketSender packetSender, ExosuitModuleEvent exosuitModuleEvent)
        {
            this.packetSender = packetSender;
            this.exosuitModuleEvent = exosuitModuleEvent;
        }

        public override void Process(ExosuitArmActionPacket packet)
        {
            Optional<GameObject> opGameObject = GuidHelper.GetObjectFrom(packet.ArmGuid);
            if(opGameObject.IsEmpty())
            {
                Log.Error("找不到外套臂");
                return;
            }
            GameObject gameObject = opGameObject.Get();
            switch (packet.TechType)
            {

                case TechType.ExosuitClawArmModule:
                    exosuitModuleEvent.UseClaw(gameObject.GetComponent<ExosuitClawArm>(), packet.ArmAction);
                    break;
                case TechType.ExosuitDrillArmModule:
                    exosuitModuleEvent.UseDrill(gameObject.GetComponent<ExosuitDrillArm>(), packet.ArmAction);
                    break;
                case TechType.ExosuitGrapplingArmModule:
                    exosuitModuleEvent.UseGrappling(gameObject.GetComponent<ExosuitGrapplingArm>(), packet.ArmAction, packet.OpVector);
                    break;
                case TechType.ExosuitTorpedoArmModule:                    
                        exosuitModuleEvent.UseTorpedo(gameObject.GetComponent<ExosuitTorpedoArm>(), packet.ArmAction, packet.OpVector, packet.OpRotation);                    
                    break;
                default:
                    Log.Error("无法获取手臂技术，句柄: " + packet.TechType + " 动作: " + packet.ArmAction + " guid: " + packet.ArmGuid);
                    break;
            }
            
        }
    }
}
