﻿using System;
using NitroxClient.Communication.Abstract;

namespace NitroxClient.Communication.MultiplayerSession.ConnectionState
{
    public abstract class ConnectionNegotiatedState : CommunicatingState
    {
        public override void NegotiateReservation(IMultiplayerSessionConnectionContext sessionConnectionContext)
        {
            throw new InvalidOperationException("无法在当前状态下协商会话连接");
        }
    }
}
