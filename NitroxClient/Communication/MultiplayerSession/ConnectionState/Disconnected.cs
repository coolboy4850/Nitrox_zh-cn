﻿using System;
using NitroxClient.Communication.Abstract;
using NitroxClient.Communication.Exceptions;
using NitroxModel.Helper;
using NitroxModel.Packets;

namespace NitroxClient.Communication.MultiplayerSession.ConnectionState
{
    public class Disconnected : IMultiplayerSessionConnectionState
    {
        private object stateLock = new object();
        public MultiplayerSessionConnectionStage CurrentStage => MultiplayerSessionConnectionStage.Disconnected;

        public void NegotiateReservation(IMultiplayerSessionConnectionContext sessionConnectionContext)
        {
            lock (stateLock)
            {
                ValidateState(sessionConnectionContext);

                IClient client = sessionConnectionContext.Client;
                string ipAddress = sessionConnectionContext.IpAddress;
                int port = sessionConnectionContext.ServerPort;
                StartClient(ipAddress, client,port);
                EstablishSessionPolicy(sessionConnectionContext, client);
            }
        }

        private void ValidateState(IMultiplayerSessionConnectionContext sessionConnectionContext)
        {
            ValidateClient(sessionConnectionContext);

            try
            {
                Validate.NotNull(sessionConnectionContext.IpAddress);
            }
            catch (ArgumentNullException ex)
            {
                throw new InvalidOperationException("上下文缺少IP地址", ex);
            }
        }

        private static void ValidateClient(IMultiplayerSessionConnectionContext sessionConnectionContext)
        {
            try
            {
                Validate.NotNull(sessionConnectionContext.Client);
            }
            catch (ArgumentNullException ex)
            {
                throw new InvalidOperationException("在尝试协商会话保留之前，必须在连接上下文上设置客户端", ex);
            }
        }

        private static void StartClient(string ipAddress, IClient client, int port)
        {
            if (!client.IsConnected)
            {
                client.Start(ipAddress,port);

                if (!client.IsConnected)
                {
                    throw new ClientConnectionFailedException("客户端无法连接，但未提供原因");
                }
            }
        }

        private static void EstablishSessionPolicy(IMultiplayerSessionConnectionContext sessionConnectionContext, IClient client)
        {
            string policyRequestCorrelationId = Guid.NewGuid().ToString();

            MultiplayerSessionPolicyRequest requestPacket = new MultiplayerSessionPolicyRequest(policyRequestCorrelationId);
            client.Send(requestPacket);

            EstablishingSessionPolicy nextState = new EstablishingSessionPolicy(policyRequestCorrelationId);
            sessionConnectionContext.UpdateConnectionState(nextState);
        }

        public void JoinSession(IMultiplayerSessionConnectionContext sessionConnectionContext)
        {
            throw new InvalidOperationException("在与服务器协商保留之前，无法加入会话");
        }

        public void Disconnect(IMultiplayerSessionConnectionContext sessionConnectionContext)
        {
            throw new InvalidOperationException("未连接到多人游戏服务器");
        }
    }
}
