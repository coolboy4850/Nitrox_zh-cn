﻿using System.Reflection;
using NitroxModel.Helper;
using UnityEngine;

namespace NitroxClient.GameLogic.ItemDropActions
{
    public class ConstructorDropAction : ItemDropAction
    {
        public override void ProcessDroppedItem(GameObject gameObject)
        {
            Constructor constructor = gameObject.GetComponent<Constructor>();
            Validate.NotNull(constructor, "GameObject没有相应的构造函数组件");

            MethodInfo deploy = typeof(Constructor).GetMethod("Deploy", BindingFlags.NonPublic | BindingFlags.Instance);
            Validate.NotNull(deploy, "在类的构造函数中找不到 'Constructor' 方法");

            deploy.Invoke(constructor, new object[] { true });
        }
    }
}
