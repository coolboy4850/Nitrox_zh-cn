﻿using System.Collections.Generic;
using NitroxClient.Communication.Abstract;
using NitroxModel.Packets;
using NitroxModel.DataStructures;
using NitroxModel.Logger;

namespace NitroxClient.GameLogic
{
    public class SimulationOwnership
    {
        public delegate void LockRequestCompleted(string guid, bool lockAquired);

        private readonly IMultiplayerSession muliplayerSession;
        private readonly IPacketSender packetSender;
        private readonly Dictionary<string, SimulationLockType> simulatedGuidsByLockType = new Dictionary<string, SimulationLockType>();
        private readonly Dictionary<string, LockRequestCompleted> completeFunctionsByGuid = new Dictionary<string, LockRequestCompleted>();
        
        public SimulationOwnership(IMultiplayerSession muliplayerSession, IPacketSender packetSender)
        {
            this.muliplayerSession = muliplayerSession;
            this.packetSender = packetSender;
        }

        public bool HasAnyLockType(string guid)
        {
            return simulatedGuidsByLockType.ContainsKey(guid);
        }

        public bool HasExclusiveLock(string guid)
        {
            SimulationLockType activeLockType;

            if (simulatedGuidsByLockType.TryGetValue(guid, out activeLockType))
            {
                return (activeLockType == SimulationLockType.EXCLUSIVE);
            }

            return false;
        }

        public void RequestSimulationLock(string guid, SimulationLockType lockType, LockRequestCompleted whenCompleted)
        {
            SimulationOwnershipRequest ownershipRequest = new SimulationOwnershipRequest(muliplayerSession.Reservation.PlayerId, guid, lockType);
            packetSender.Send(ownershipRequest);
            completeFunctionsByGuid[guid] = whenCompleted;
        }

        public void ReceivedSimulationLockResponse(string guid, bool lockAquired, SimulationLockType lockType)
        {
            Log.Info("收到锁定响应, guid: " + guid + " " + lockAquired + " " + lockType);

            if (lockAquired)
            {
                SimulateGuid(guid, lockType);
            }

            LockRequestCompleted requestCompleted = null;

            if (completeFunctionsByGuid.TryGetValue(guid, out requestCompleted) && requestCompleted != null)
            {
                completeFunctionsByGuid.Remove(guid);
                requestCompleted(guid, lockAquired);
            }
            else
            {
                Log.Warn("没有未完成的模拟请求 " + guid + " 可能有多个未完成的请求?");
            }
        }

        public void SimulateGuid(string guid, SimulationLockType lockType)
        {
            simulatedGuidsByLockType[guid] = lockType;
        }

        public void StopSimulatingGuid(string guid)
        {
            simulatedGuidsByLockType.Remove(guid);
        }
    }
}
