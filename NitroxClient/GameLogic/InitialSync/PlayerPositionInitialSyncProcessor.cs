﻿using NitroxClient.GameLogic.Helper;
using NitroxClient.GameLogic.InitialSync.Base;
using NitroxModel.DataStructures.Util;
using NitroxModel.Helper;
using NitroxModel.Logger;
using NitroxModel.Packets;
using UnityEngine;

namespace NitroxClient.GameLogic.InitialSync
{
    public class PlayerPositionInitialSyncProcessor : InitialSyncProcessor
    {
        public PlayerPositionInitialSyncProcessor()
        {
            DependentProcessors.Add(typeof(PlayerInitialSyncProcessor)); // Make sure the player is configured
            DependentProcessors.Add(typeof(BuildingInitialSyncProcessor)); // Players can be spawned in buildings
            DependentProcessors.Add(typeof(EscapePodInitialSyncProcessor)); // Players can be spawned in escapePod
            DependentProcessors.Add(typeof(VehicleInitialSyncProcessor)); // Players can be spawned in vehicles
        }

        public override void Process(InitialPlayerSync packet)
        {
            Vector3 position = packet.PlayerSpawnData;
            Optional<string> subRootGuid = packet.PlayerSubRootGuid;

            if (!(position.x == 0 && position.y == 0 && position.z == 0))
            {
                Player.main.SetPosition(position);
            }

            if (subRootGuid.IsPresent())
            {
                Optional<GameObject> sub = GuidHelper.GetObjectFrom(subRootGuid.Get());

                if (sub.IsPresent())
                {
                    SubRoot root = sub.Get().GetComponent<SubRoot>();
                    // Player position is relative to a subroot if in a subroot
                    if (root != null && !root.isBase)
                    {                        
                        Player.main.SetCurrentSub(root);
                        Quaternion vehicleAngle = root.transform.rotation;
                        position = vehicleAngle * position;
                        position = position + root.transform.position;
                        Player.main.SetPosition(position);
                    }
                    else
                    {
                        Log.Error("找不到 guid 为的子程序的玩家的子程序: " + subRootGuid.Get());
                    }
                }
                else
                {
                    Log.Error("无法将玩家生成到 guid 为的子程序中: " + subRootGuid.Get());
                }
            }
        }
    }
}
