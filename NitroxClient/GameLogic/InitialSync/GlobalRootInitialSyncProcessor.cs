﻿using NitroxClient.GameLogic.InitialSync.Base;
using NitroxModel.Logger;
using NitroxModel.Packets;

namespace NitroxClient.GameLogic.InitialSync
{
    public class GlobalRootInitialSyncProcessor : InitialSyncProcessor
    {
        private readonly Entities entities;

        public GlobalRootInitialSyncProcessor(Entities entities)
        {
            this.entities = entities;
        }

        public override void Process(InitialPlayerSync packet)
        {
            Log.Info("接收到初始同步数据包 " + packet.GlobalRootEntities.Count + " 全局根实体");
            entities.Spawn(packet.GlobalRootEntities);
        }
    }
}
