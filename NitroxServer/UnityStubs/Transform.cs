﻿using ProtoBufNet;

namespace NitroxServer.UnityStubs
{
    [ProtoContract]
    public class Transform
    {
        [ProtoMember(1)]
        public Vector3 Position { get; }

        [ProtoMember(2)]
        public Quaternion Rotation { get; }

        [ProtoMember(3)]
        public Vector3 Scale { get; }

        public Transform(Vector3 position, Vector3 scale, Quaternion rotation)
        {
            Position = position;
            Scale = scale;
            Rotation = rotation;
        }

        public override string ToString()
        {
            return "[传输位置: " + Position + " 规模: " + Scale + " 旋转: " + Rotation + "]";
        }
    }
}
