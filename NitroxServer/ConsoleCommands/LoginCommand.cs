﻿using System.Text.RegularExpressions;
using NitroxServer.ConsoleCommands.Abstract;
using NitroxServer.GameLogic.Players;
using NitroxModel.DataStructures.GameLogic;
using NitroxServer.GameLogic;
using NitroxModel.DataStructures.Util;
using NitroxModel.Logger;
using NitroxServer.ConfigParser;

namespace NitroxServer.ConsoleCommands
{
    internal class LoginCommand : Command
    {
        private readonly PlayerData playerData;
        private readonly PlayerManager playerManager;
        private readonly ServerConfig serverConfig;

        public LoginCommand(PlayerData playerData, PlayerManager playerManager, ServerConfig serverConfig) : base("login", Perms.PLAYER, "<密码>")
        {
            this.playerData = playerData;
            this.playerManager = playerManager;
            this.serverConfig = serverConfig;
        }

        public override void RunCommand(string[] args, Optional<Player> player)
        {
            string pass = args[0];
            string message;
            string playerName = player.Get().Name;

            if (pass == serverConfig.AdminPassword)
            {
                if (playerData.UpdatePlayerPermissions(playerName, Perms.ADMIN))
                {
                    message = "已更新管理员的权限 " + playerName;
                }
                else
                {
                    message = "无法更新权限 " + playerName;
                }
            }
            else
            {
                message = "密码不正确";
            }
            Log.Info(message);
            SendServerMessageIfPlayerIsPresent(player, message);
        }

        public override bool VerifyArgs(string[] args)
        {
            return args.Length == 1;
        }
    }
}
