﻿using System.Timers;
using NitroxModel.Logger;
using NitroxServer.Serialization.World;
using NitroxServer.ConfigParser;
using System.Configuration;

namespace NitroxServer
{
    public class Server
    {
        private readonly Timer saveTimer;
        private Communication.NetworkingLayer.NitroxServer server;
        private readonly World world;
        private readonly WorldPersistence worldPersistence;
        public bool IsRunning { get; private set; }
        private bool IsSaving;
        public static Server Instance { get; private set; }

        public Server(WorldPersistence worldPersistence, World world, ServerConfig serverConfig, Communication.NetworkingLayer.NitroxServer server)
        {
            if (ConfigurationManager.AppSettings.Count == 0)
            {
                Log.Warn("Nitrox 服务器不能读取配置文件");
            }
            Instance = this;
            this.worldPersistence = worldPersistence;
            this.world = world;
            this.server = server;
            
            saveTimer = new Timer();
            saveTimer.Interval = serverConfig.SaveInterval;
            saveTimer.AutoReset = true;
            saveTimer.Elapsed += delegate { Save(); };
        }

        public void Save()
        {
            if (IsSaving)
            {
                return;
            }
            IsSaving = true;
            worldPersistence.Save(world);
            IsSaving = false;
        }

        public void Start()
        {
            IsRunning = true;
            IpLogger.PrintServerIps();
            server.Start();
            Log.Info("Nitrox 服务器已启动（若要停止，请输入 stop ）");
            EnablePeriodicSaving();
        }

        public void Stop()
        {
            Log.Info("Nitrox 服务器正在停止...");
            DisablePeriodicSaving();
            Save();
            server.Stop();
            Log.Info("Nitrox 服务器已停止");
            IsRunning = false;
        }

        private void EnablePeriodicSaving()
        {
            saveTimer.Start();
        }

        private void DisablePeriodicSaving()
        {
            saveTimer.Stop();
        }
    }
}
