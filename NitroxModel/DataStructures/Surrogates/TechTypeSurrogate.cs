﻿using System.Runtime.Serialization;
using TechTypeModel = NitroxModel.DataStructures.TechType;

namespace NitroxModel.DataStructures.Surrogates
{
    public class TechTypeSurrogate : SerializationSurrogate<TechTypeModel>
    {
        protected override void GetObjectData(TechTypeModel techType, SerializationInfo info)
        {
            info.AddValue("名字", techType.Name);
        }

        protected override TechTypeModel SetObjectData(TechTypeModel techType, SerializationInfo info)
        {
            techType.Name = info.GetString("名字");
            return techType;
        }
    }
}
