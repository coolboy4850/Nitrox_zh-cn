﻿using System;
using System.Collections.Generic;
using ProtoBufNet;
using UnityEngine;

namespace NitroxModel.DataStructures.GameLogic
{
    [Serializable]
    [ProtoContract]
    public class EscapePodModel
    {
        [ProtoMember(1)]
        public string Guid { get; set; }

        [ProtoMember(2)]
        public Vector3 Location { get; set; }

        [ProtoMember(3)]
        public string FabricatorGuid { get; set; }

        [ProtoMember(4)]
        public string MedicalFabricatorGuid { get; set; }

        [ProtoMember(5)]
        public string StorageContainerGuid { get; set; }

        [ProtoMember(6)]
        public string RadioGuid { get; set; }

        [ProtoMember(7)]
        public List<ushort> AssignedPlayers { get; set; } = new List<ushort>();

        [ProtoMember(8)]
        public bool Damaged { get; set; }

        [ProtoMember(9)]
        public bool RadioDamaged { get; set; }

        public void InitEscapePodModel(string guid, Vector3 location, string fabricatorGuid, string medicalFabricatorGuid, string storageContainerGuid, string radioGuid, bool damaged, bool radioDamaged)
        {
            Guid = guid;
            Location = location;
            FabricatorGuid = fabricatorGuid;
            MedicalFabricatorGuid = medicalFabricatorGuid;
            StorageContainerGuid = storageContainerGuid;
            RadioGuid = radioGuid;
            Damaged = damaged;
            RadioDamaged = radioDamaged;
        }

        public override string ToString()
        {
            string toString = "[逃生舱模型 - Guid: " + Guid + " 位置:" + Location + " 制造商Guid: " + FabricatorGuid + "药物制造商Guid: " + MedicalFabricatorGuid + " 储存容器Guid: " + StorageContainerGuid + " 无线电Guid: " + RadioGuid + " 指定玩家: {";

            foreach (ushort playerId in AssignedPlayers)
            {
                toString += playerId + " ";
            }

            return toString + "} 损坏: " + Damaged + " 放射性损伤: " + RadioDamaged + "]";
        }
    }
}
