﻿using System;

namespace NitroxModel.DataStructures.GameLogic
{
    [Serializable]
    public class InteractiveChildObjectIdentifier
    {
        public string Guid { get; }
        public string GameObjectNamePath { get; }

        public InteractiveChildObjectIdentifier(string guid, string gameObjectNamePath)
        {
            Guid = guid;
            GameObjectNamePath = gameObjectNamePath;
        }

        public override string ToString()
        {
            return "[交互式对象标识符 - Guid: " + Guid + " 游戏对象名称路径: " + GameObjectNamePath + "]";
        }
    }
}
