﻿using System;
using NitroxModel.DataStructures;

namespace NitroxModel.Packets
{
    [Serializable]
    public class KnownTechEntryAdd : Packet
    {
        public TechType TechType;
        public bool Verbose;

        public KnownTechEntryAdd(TechType techType, bool verbose)
        {
            TechType = techType;
            Verbose = verbose;
        }
        public override string ToString()
        {
            return "[已知技术条目添加 - 技术类型: " + TechType + " 详细: " + Verbose + "]";
        }
    }    
}
