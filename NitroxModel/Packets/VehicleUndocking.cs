﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class VehicleUndocking : Packet
    {
        public string VehicleGuid { get; }
        public string DockGuid { get; }
        public ushort PlayerId { get; }

        public VehicleUndocking(string vehicleGuid, string dockGuid, ushort playerId)
        {
            VehicleGuid = vehicleGuid;
            DockGuid = dockGuid;
            PlayerId = playerId;
        }

        public override string ToString()
        {
            return "[正在卸载交通工具 交通工具Guid: " + VehicleGuid + " 锁止Guid: " + DockGuid + " 玩家Id: " + PlayerId + "]";
        }
    }
}
