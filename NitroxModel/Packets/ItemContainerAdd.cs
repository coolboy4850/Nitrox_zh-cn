﻿using NitroxModel.DataStructures.GameLogic;
using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class ItemContainerAdd : Packet
    {
        public ItemData ItemData { get; }

        public ItemContainerAdd(ItemData itemData)
        {
            ItemData = itemData;
        }

        public override string ToString()
        {
            return "[集装箱附加项目的项目数据: " + ItemData + "]";
        }
    }
}
