﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class PDAEncyclopediaEntryAdd : Packet
    {
        public string Key;

        public PDAEncyclopediaEntryAdd(string key)
        {
            Key = key;
        }

        public override string ToString()
        {
            return "[PDA百科全书条目添加 - 条目: " + Key + "]";
        }
    }
}
