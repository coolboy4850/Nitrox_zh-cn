﻿using System;
using NitroxModel.DataStructures;

namespace NitroxModel.Packets
{
    [Serializable]
    public class PDAEntryRemove : Packet
    {
        public TechType TechType;
        public float Progress;
        public int Unlocked;

        public PDAEntryRemove(TechType techType, float progress, int unlocked)
        {
            TechType = techType;
            Progress = progress;
            Unlocked = unlocked;
        }

        public override string ToString()
        {
            return "[删除PDA条目 - 技术类型: " + TechType + " 进度: " + Progress + " 卸载: " + Unlocked + "]";
        }
    }
}
