﻿using System;
using NitroxModel.DataStructures;

namespace NitroxModel.Packets
{
    [Serializable]
    public class FabricatorBeginCrafting : Packet
    {
        public string FabricatorGuid { get; }
        public TechType TechType { get; }
        public float Duration { get; }

        public FabricatorBeginCrafting(string fabricatorGuid, TechType techType, float duration)
        {
            FabricatorGuid = fabricatorGuid;
            TechType = techType;
            Duration = duration;
        }

        public override string ToString()
        {
            return "[制造商注册-制造商Guid: " + FabricatorGuid + " 技术类型: " + TechType + " 持续时间: " + Duration + "]";
        }
    }
}
