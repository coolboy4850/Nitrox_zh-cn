﻿using System;
using NitroxModel.Networking;

namespace NitroxModel.Packets
{
    [Serializable]
    public class PlayerStats : Packet
    {
        public ushort PlayerId { get; }
        public float Oxygen { get; }
        public float MaxOxygen { get; }
        public float Health { get; }
        public float Food { get; }
        public float Water { get; }

        public PlayerStats(ushort playerId, float oxygen, float maxOxygen, float health, float food, float water)
        {
            PlayerId = playerId;
            Oxygen = oxygen;
            MaxOxygen = maxOxygen;
            Health = health;
            Food = food;
            Water = water;
            DeliveryMethod = NitroxDeliveryMethod.DeliveryMethod.UnreliableSequenced;
            UdpChannel = UdpChannelId.PLAYER_STATS;
        }

        public override string ToString()
        {
            return "[玩家状态 - 玩家Id: " + PlayerId + " 氧气: " + Oxygen + " 最大氧气:" + MaxOxygen + " 健康: " + Health + " 食物: " + Food + " 水: " + Water + "]";
        }
    }
}
