﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class VehicleOnPilotModeChanged : Packet
    {
        public string VehicleGuid { get; }
        public ushort PlayerId { get; }
        public bool IsPiloting { get; }

        public VehicleOnPilotModeChanged(string vehicleGuid, ushort playerId, bool isPiloting)
        {
            VehicleGuid = vehicleGuid;
            PlayerId = playerId;
            IsPiloting = isPiloting;
        }

        public override string ToString()
        {
            return "[交通工具处于引导模式已更改 - 交通工具Guid: " + VehicleGuid + " 玩家Id: " + PlayerId + " 是否引导: " + IsPiloting + "]";
        }
    }
}
