﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class PDAEncyclopediaEntryRemove : Packet
    {
        public string Key;

        public PDAEncyclopediaEntryRemove(string key)
        {
            Key = key;
        }

        public override string ToString()
        {
            return "[删除PDA百科全书条目 - 条目: " + Key + "]";
        }
    }
}
