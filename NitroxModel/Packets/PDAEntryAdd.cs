﻿using System;
using NitroxModel.DataStructures;

namespace NitroxModel.Packets
{
    [Serializable]
    public class PDAEntryAdd : Packet
    {
        public TechType TechType;
        public float Progress;
        public int Unlocked;

        public PDAEntryAdd(TechType techType, float progress, int unlocked)
        {
            TechType = techType;
            Progress = progress;
            Unlocked = unlocked;
        }

        public override string ToString()
        {
            return "[PDA条目添加 - 技术类型: " + TechType + " 进度: " + Progress + " 卸载: " + Unlocked + "]";
        }
    }
}
