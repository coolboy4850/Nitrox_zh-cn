﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class ItemContainerRemove : Packet
    {
        public string OwnerGuid { get; }
        public string ItemGuid { get; }

        public ItemContainerRemove(string ownerGuid, string itemGuid)
        {
            OwnerGuid = ownerGuid;
            ItemGuid = itemGuid;
        }

        public override string ToString()
        {
            return "[项目容器移除所有者Guid: " + OwnerGuid + " 项目Guid: " + ItemGuid + "]";
        }
    }
}
