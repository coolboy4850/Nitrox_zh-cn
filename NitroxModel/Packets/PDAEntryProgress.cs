﻿using System;
using NitroxModel.DataStructures;

namespace NitroxModel.Packets
{
    [Serializable]
    public class PDAEntryProgress : Packet
    {
        public TechType TechType;
        public float Progress;
        public int Unlocked;

        public PDAEntryProgress(TechType techType, float progress, int unlocked)
        {
            TechType = techType;
            Progress = progress;
            Unlocked = unlocked;
        }

        public override string ToString()
        {
            return "[PDA输入进度 - 技术类型: " + TechType + " 进度: " + Progress + " 卸载: " + Unlocked + "]";
        }
    }
}
