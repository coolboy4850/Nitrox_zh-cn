﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class StoryEventSend : Packet
    {
        public StoryEventType StoryEventType { get; }
        public string Key { get; }

        public StoryEventSend(StoryEventType storyEventType, string key = "") : base()
        {
            StoryEventType = storyEventType;
            Key = key;
        }

        public override string ToString()
        {
            return "[故事事件发送 - 故事事件类型: " + StoryEventType + " 条目: " + Key + "]";
        }
    }

    public enum StoryEventType
    {
        PDA,
        Radio,
        Encyclopedia,
        Story,
        Extra
    }
}
