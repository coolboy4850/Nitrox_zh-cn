﻿using NitroxModel.DataStructures;
using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class SimulationOwnershipResponse : Packet
    {
        public string Guid { get; }
        public bool LockAquired { get; }
        public SimulationLockType LockType { get; }

        public SimulationOwnershipResponse(string guid, bool lockAquired, SimulationLockType lockType)
        {
            Guid = guid;
            LockAquired = lockAquired;
            LockType = lockType;
        }

        public override string ToString()
        {
            return "[模拟所有权响应 - Guid: " + Guid + " 锁定请求: " + LockAquired + " 锁定类型: " + LockType + "]";
        }
    }
}
