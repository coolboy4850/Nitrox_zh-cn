﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class VehicleDocking : Packet
    {
        public string VehicleGuid { get; }
        public string DockGuid { get; }
        public ushort PlayerId { get; }

        public VehicleDocking(string vehicleGuid, string dockGuid, ushort playerId)
        {
            VehicleGuid = vehicleGuid;
            DockGuid = dockGuid;
            PlayerId = playerId;
        }

        public override string ToString()
        {
            return "[交通工具锁止 交通工具Guid: " + VehicleGuid + " 锁止Guid: " + DockGuid + " 玩家Id: " + PlayerId + "]";
        }
    }
}
