﻿using System;

namespace NitroxModel.Packets
{
    [Serializable]
    public class VehicleDestroyed : Packet
    {
        public string Guid { get; }
        public string PlayerName { get; }
        public bool GetPilotingMode { get; }

        public VehicleDestroyed(string guid, string playerName, bool getPilotingMode)
        {
            Guid = guid;
            PlayerName = playerName;
            GetPilotingMode = getPilotingMode;
        }

        public override string ToString()
        {
            return "[移除交通工具 - 交通工具 Guid: " + Guid + " 玩家名字: " + PlayerName + " 获取引导模式: " + GetPilotingMode + "]";
        }
    }
}
