﻿using System;
using UnityEngine;
using NitroxModel.DataStructures;

namespace NitroxModel.Packets
{
    [Serializable]
    public class SeamothModulesAction : Packet
    {
        public TechType TechType { get; }
        public int SlotID { get; }
        public string Guid { get; }
        public Vector3 Forward { get; }
        public Quaternion Rotation { get; }

        public SeamothModulesAction(TechType techType, int slotID, string guid, Vector3 forward, Quaternion rotation)
        {
            TechType = techType;
            SlotID = slotID;
            Guid = guid;
            Forward = forward;
            Rotation = rotation;
        }
        public override string ToString()
        {
            return "[海蛾模块动作 - 技术类型: " + TechType + " 插入ID: " + SlotID + " Guid:" + Guid + " 向前: " + Forward + " 旋转: " + Rotation + "]";
        }
    }
}
