﻿using System;
using UnityEngine;
using Lidgren.Network;
using NitroxModel.Networking;

namespace NitroxModel.Packets
{
    [Serializable]
    public class Movement : Packet
    {
        public ushort PlayerId { get; }
        public Vector3 Position { get; }
        public Vector3 Velocity { get; }
        public Quaternion BodyRotation { get; }
        public Quaternion AimingRotation { get; }

        public Movement(ushort playerId, Vector3 position, Vector3 velocity, Quaternion bodyRotation, Quaternion aimingRotation)
        {
            PlayerId = playerId;
            Position = position;
            Velocity = velocity;
            BodyRotation = bodyRotation;
            AimingRotation = aimingRotation;
            DeliveryMethod = NitroxDeliveryMethod.DeliveryMethod.UnreliableSequenced;
            UdpChannel = UdpChannelId.PLAYER_MOVEMENT;
        }

        public override string ToString()
        {
            return "[移动-玩家Id: " + PlayerId + " 位置: " + Position + " 速度: " + Velocity + " 身体旋转: " + BodyRotation + " 摄像头旋转: " + AimingRotation + "]";
        }
    }
}
