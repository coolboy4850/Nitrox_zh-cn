﻿using System;
using System.Collections.Generic;
using NitroxModel.DataStructures.GameLogic;
using NitroxModel.DataStructures.Util;
using UnityEngine;

namespace NitroxModel.Packets
{
    [Serializable]
    public class VehicleCreated : Packet
    {
        public string PlayerName { get; }
        public VehicleModel CreatedVehicle { get; }

        public VehicleCreated(VehicleModel createdVehicle, string playerName)
        {
            CreatedVehicle = createdVehicle;
            PlayerName = playerName;
        }

        public override string ToString()
        {
            return "[交通工具已创建 - 交通工具 Guid: " + CreatedVehicle.Guid + " 玩家名称: " + PlayerName + " 交通工具类型: " + CreatedVehicle.TechType + "]";
        }
    }
}
