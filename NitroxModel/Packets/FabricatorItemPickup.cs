﻿using System;
using NitroxModel.DataStructures;

namespace NitroxModel.Packets
{
    [Serializable]
    public class FabricatorItemPickup : Packet
    {
        public string FabricatorGuid { get; }
        public TechType TechType { get; }

        public FabricatorItemPickup(string fabricatorGuid, TechType techType)
        {
            FabricatorGuid = fabricatorGuid;
            TechType = techType;
        }

        public override string ToString()
        {
            return "[制造商取件-制造商Guid: " + FabricatorGuid + " 技术类型: " + TechType + "]";
        }
    }
}
