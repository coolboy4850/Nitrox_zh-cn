﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NitroxModel.DataStructures.GameLogic;

namespace NitroxModel.Packets
{
    [Serializable]
    public class StorageSlotItemAdd : Packet
    {
        public ItemData ItemData { get; }

        public StorageSlotItemAdd(ItemData itemData)
        {
            ItemData = itemData;
        }

        public override string ToString()
        {
            return "[添加存储槽项 项目数据: " + ItemData + "]";
        }
    }
}
