﻿using System;
using System.ComponentModel;
using System.Text;

namespace NitroxModel.MultiplayerSession
{
    [Flags]
    public enum MultiplayerSessionReservationState
    {
        Reserved = 0,
        Rejected = 1 << 0,

        [Description("玩家名称已在使用中，请使用其他名称重试")]
        UniquePlayerNameConstraintViolated = 1 << 1,

        // These are all intended for future use. Maybe YAGNI, but this is where we should look to expand upon server reservations
        [Description("服务器当前已满负荷。请稍后再试")]
        ServerPlayerCapacityReached = 1 << 2,

        [Description("您为服务器提供的密码不正确")]
        AuthenticationFailed = 1 << 3
    }

    public static class MultiplayerSessionReservationStateExtensions
    {
        public static bool HasStateFlag(this MultiplayerSessionReservationState currentState, MultiplayerSessionReservationState checkedState)
        {
            return (currentState & checkedState) == checkedState;
        }

        public static string Describe(this MultiplayerSessionReservationState currentState)
        {
            StringBuilder descriptionBuilder = new StringBuilder();

            foreach (string reservationStateName in Enum.GetNames(typeof(MultiplayerSessionReservationState)))
            {
                MultiplayerSessionReservationState reservationState = (MultiplayerSessionReservationState)Enum.Parse(typeof(MultiplayerSessionReservationState), reservationStateName);
                if (currentState.HasStateFlag(reservationState))
                {
                    DescriptionAttribute descriptionAttribute = reservationState.GetAttribute<DescriptionAttribute>();

                    if (!string.IsNullOrEmpty(descriptionAttribute?.Description))
                    {
                        descriptionBuilder.AppendLine(descriptionAttribute.Description);
                    }
                }
            }

            return descriptionBuilder.ToString();
        }
    }
}
