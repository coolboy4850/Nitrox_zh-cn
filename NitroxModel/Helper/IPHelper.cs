﻿using System.IO;
using System.Net;

namespace NitroxModel.Helper
{
    public static class IPHelper
    {
        //"Hacky way" to get the public IP
        //Should definitely be reworked.
        public static string GetPublicIP()
        {
            /*WebRequest req = WebRequest.Create("http://checkip.dyndns.org");
            using (StreamReader sr = new StreamReader(req.GetResponse().GetResponseStream()))
            {
                return sr.ReadToEnd().Trim().Split(':')[1].Substring(1).Split('<')[0];
            }*/

           
            WebClient MyWebClient = new WebClient();
            MyWebClient.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36");
            MyWebClient.Credentials = CredentialCache.DefaultCredentials;//获取或设置用于向Internet资源的请求进行身份验证的网络凭据
            byte[] pageData = MyWebClient.DownloadData("http://www.net.cn/static/customercare/yourip.asp"); //从指定网站下载数据
            string pageHtml = System.Text.Encoding.Default.GetString(pageData);  //如果获取网站页面采用的是GB2312，则 使用这句
            System.Text.RegularExpressions.Match m = System.Text.RegularExpressions.Regex.Match(pageHtml, @"\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}");
            return pageHtml;
           
        }
    }
}
