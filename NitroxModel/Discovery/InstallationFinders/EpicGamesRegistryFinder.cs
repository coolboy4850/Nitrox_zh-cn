﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Win32;
using NitroxModel.DataStructures.Util;

namespace NitroxModel.Discovery.InstallationFinders
{
    public class EpicGamesRegistryFinder : IFindGameInstallation
    {
        private const string BASE_REGISTRY_KEY = @"Local Settings\Software\Microsoft\Windows\Shell\MuiCache";

        public Optional<string> FindGame(List<string> errors)
        {
            using (RegistryKey key = Registry.ClassesRoot.OpenSubKey(BASE_REGISTRY_KEY))
            {
                if (key == null)
                {
                    errors.Add($"未找到注册表项 '{BASE_REGISTRY_KEY}' ，无法从Epic Games的启动程序确定Subnautica安装目录");
                    return Optional<string>.Empty();
                }

                string subnauticaKeyName = key.GetValueNames().FirstOrDefault(name => name.IndexOf("subnautica.exe", StringComparison.OrdinalIgnoreCase) >= 0);
                if (string.IsNullOrEmpty(subnauticaKeyName))
                {
                    errors.Add($"注册表项 '{Path.Combine(key.Name, "subnautica.exe")}' 不存在，无法从Epic Games的启动程序确定Subnautica安装目录");
                    return Optional<string>.Empty();
                }

                // Replace FriendAppName if added to key on Win7+EpicGames Subnautica installation.
                string guessedInstallDirectory = Directory.GetParent(subnauticaKeyName.Replace(".FriendlyAppName", "")).FullName;

                if (!Directory.Exists(guessedInstallDirectory))
                {
                    errors.Add($"Path '{guessedInstallDirectory}' 在Epic Games启动程序的注册表项中找到的不是目录");
                    return Optional<string>.Empty();
                }

                if (!Directory.GetFiles(guessedInstallDirectory, "*.exe").Any(fileName => fileName.IndexOf("subnautica.exe", StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    errors.Add($"Path '{guessedInstallDirectory}' 在Epic Games游戏启动程序的注册表项中找到的不包含深海迷航可执行文件。");
                    return Optional<string>.Empty();
                }

                return Optional<string>.Of(guessedInstallDirectory);
            }
        }
    }
}
