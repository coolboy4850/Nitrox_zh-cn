﻿using System.Collections.Generic;
using System.IO;
using NitroxModel.DataStructures.Util;

namespace NitroxModel.Discovery.InstallationFinders
{
    /// <summary>
    ///     Tries to read a local file that contains the installation directory of Subnautica.
    /// </summary>
    public class ConfigFileGameFinder : IFindGameInstallation
    {
        private const string FILENAME = "path.txt";

        public Optional<string> FindGame(List<string> errors)
        {
            if (!File.Exists(FILENAME))
            {
                errors.Add($@"未设置游戏安装目录配置文件。 创建一个 '{FILENAME}' 在目录: '{Directory.GetCurrentDirectory()}' 带有深海迷航安装目录的路径");
                return Optional<string>.Empty();
            }

            string path = File.ReadAllText(FILENAME).Trim();
            if (string.IsNullOrEmpty(path))
            {
                errors.Add($@"配置文件 {Path.GetFullPath(FILENAME)} 发现为空。请输入深海迷航安装的路径");
                return Optional<string>.Empty();
            }

            return Optional<string>.Of(path);
        }
    }
}
