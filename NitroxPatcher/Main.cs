﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Harmony;
using NitroxClient;
using NitroxClient.MonoBehaviours;
using NitroxModel.Core;
using NitroxModel.Helper;
using NitroxModel.Logger;
using NitroxPatcher.Patches;
using UnityEngine;

namespace NitroxPatcher
{
    public static class Main
    {
        private static NitroxPatch[] patches;
        private static readonly HarmonyInstance harmony = HarmonyInstance.Create("com.nitroxmod.harmony");
        private static bool isApplied;
        
        public static void Execute()
        {
            Log.EnableInGameMessages();
            
            if (patches != null)
            {
                Log.Warn("已检测到修补程序！改为调用Apply或Restore");
                return;
            }

            Log.Info("正在注册依赖项");

            // Our application's entry point. First, register client dependencies with AutoFac.
            NitroxServiceLocator.InitializeDependencyContainer(new ClientAutoFacRegistrar());

            Log.Info("正在为深海迷航打补丁...");

            // Enabling this creates a log file on your desktop (why there?), showing the emitted IL instructions.
            HarmonyInstance.DEBUG = false;

            IEnumerable<NitroxPatch> discoveredPatches = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(p => typeof(NitroxPatch).IsAssignableFrom(p) &&
                            p.IsClass && !p.IsAbstract
                      )
                .Select(Activator.CreateInstance)
                .Cast<NitroxPatch>();

            IEnumerable<IGrouping<string, NitroxPatch>> splittedPatches = discoveredPatches.GroupBy(p => p.GetType().Namespace);

            splittedPatches.First(g => g.Key == "NitroxPatcher.Patches.Persistent").ForEach(p =>
            {
                Log.Info("应用持久补丁 " + p.GetType());
                p.Patch(harmony);
            });

            patches = splittedPatches.First(g => g.Key == "NitroxPatcher.Patches").ToArray();
            Multiplayer.OnBeforeMultiplayerStart += Apply;
            Multiplayer.OnAfterMultiplayerEnd += Restore;
            Log.Info("使用完成补丁 " + Assembly.GetExecutingAssembly().FullName);

            Log.Info("启用开发人员控制台");
            DevConsole.disableConsole = false;
            Application.runInBackground = true;
            Log.Info($"Unity在后台运行设置为 {Application.runInBackground.ToString().ToUpperInvariant()}.");

            ApplyNitroxBehaviours();
        }

        public static void Apply()
        {
            Validate.NotNull(patches, "还没有发现补丁！首先运行execute()");

            if (isApplied)
            {
                return;
            }

            patches.ForEach(patch =>
            {
                Log.Info("正在应用 " + patch.GetType());
                patch.Patch(harmony);
            });

            isApplied = true;
        }

        /// <summary>
        /// If the player starts the main menu for the first time, or returns from a (multiplayer) session, get rid of all the patches if applicable.
        /// </summary>
        public static void Restore()
        {
            Validate.NotNull(patches, "还没有发现补丁！首先运行execute()");

            if (!isApplied)
            {
                return;
            }

            patches.ForEach(patch =>
            {
                Log.Info("正在恢复 " + patch.GetType());
                patch.Restore(harmony);
            });

            isApplied = false;
        }

        private static void ApplyNitroxBehaviours()
        {
            Log.Info("应用Nitrox行为..");

            GameObject nitroxRoot = new GameObject();
            nitroxRoot.name = "Nitrox";
            nitroxRoot.AddComponent<NitroxBootstrapper>();

            Log.Info("应用行为");
        }
    }
}
