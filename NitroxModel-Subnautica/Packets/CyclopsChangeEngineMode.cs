﻿using System;
using NitroxModel.Packets;

namespace NitroxModel_Subnautica.Packets
{
    [Serializable]
    public class CyclopsChangeEngineMode : Packet
    {
        public string Guid { get; }
        public CyclopsMotorMode.CyclopsMotorModes Mode { get; }

        public CyclopsChangeEngineMode(string guid, CyclopsMotorMode.CyclopsMotorModes mode)
        {
            Guid = guid;
            Mode = mode;
        }

        public override string ToString()
        {
            return "[改变独眼巨人号引擎模式 Guid: " + Guid + " 模式: " + Mode + "]";
        }
    }
}
