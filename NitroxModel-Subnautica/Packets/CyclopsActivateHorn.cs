﻿using System;
using NitroxModel.Packets;

namespace NitroxModel_Subnautica.Packets
{
    [Serializable]
    public class CyclopsActivateHorn : Packet
    {
        public string Guid { get; }

        public CyclopsActivateHorn(string guid)
        {
            Guid = guid;
        }

        public override string ToString()
        {
            return "[独眼巨人号激活号角 Guid: " + Guid + "]";
        }
    }
}
