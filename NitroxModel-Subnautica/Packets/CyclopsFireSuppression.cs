﻿using System;
using NitroxModel.Packets;

namespace NitroxModel_Subnautica.Packets
{
    [Serializable]
    public class CyclopsFireSuppression : Packet
    {
        public string Guid { get; }

        public CyclopsFireSuppression(string guid)
        {
            Guid = guid;
        }

        public override string ToString()
        {
            return "[独眼巨人号消防系统 Guid: " + Guid + "]";
        }
    }
}

