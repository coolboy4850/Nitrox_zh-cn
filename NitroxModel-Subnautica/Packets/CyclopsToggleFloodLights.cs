﻿using System;
using NitroxModel.Packets;

namespace NitroxModel_Subnautica.Packets
{
    [Serializable]
    public class CyclopsToggleFloodLights : Packet
    {
        public string Guid { get; }
        public bool IsOn { get; }

        public CyclopsToggleFloodLights(string guid, bool isOn)
        {
            Guid = guid;
            IsOn = isOn;
        }

        public override string ToString()
        {
            return "[独眼巨人号切换泛光灯 Guid: " + Guid + " 是否开启: " + IsOn + "]";
        }
    }
}
