﻿using System;
using NitroxModel.Packets;

namespace NitroxModel_Subnautica.Packets
{
    [Serializable]
    public class CyclopsToggleEngineState : Packet
    {
        public string Guid { get; }
        public bool IsOn { get; }
        public bool IsStarting { get; }

        public CyclopsToggleEngineState(string guid, bool isOn, bool isStarting)
        {
            Guid = guid;
            IsOn = isOn;
            IsStarting = isStarting;
        }

        public override string ToString()
        {
            return "[独眼巨人号发动引擎状态 Guid: " + Guid + " 是否开启: " + IsOn + " 是否启动: " + IsStarting + "]";
        }
    }
}
