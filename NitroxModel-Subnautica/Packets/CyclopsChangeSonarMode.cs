﻿using System;
using NitroxModel.Packets;

namespace NitroxModel_Subnautica.Packets
{
    [Serializable]
    public class CyclopsChangeSonarMode : Packet
    {
        public string Guid { get; }
        public bool IsOn { get; }
        
        public CyclopsChangeSonarMode(string guid, bool isOn)
        {
            Guid = guid;
            IsOn = isOn;
        }

        public override string ToString()
        {
            return "[激活独眼巨人号声纳 Guid: " + Guid + "," + "是否开启: " + IsOn + "]";
        }
    }
}
