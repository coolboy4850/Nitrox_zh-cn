﻿using System;
using ProtoBufNet;

namespace NitroxModel_Subnautica.DataStructures.GameLogic
{
    [Serializable]
    [ProtoContract]
    public class CyclopsFireData
    {
        [ProtoMember(1)]
        public string FireGuid { get; set; }

        [ProtoMember(2)]
        public string CyclopsGuid { get; set; }

        [ProtoMember(3)]
        public CyclopsRooms Room { get; set; }

        [ProtoMember(4)]
        public int NodeIndex { get; set; }

        public CyclopsFireData()
        {

        }

        public CyclopsFireData(string fireGuid, string cyclopsGuid, CyclopsRooms room, int nodeIndex)
        {
            FireGuid = fireGuid;
            CyclopsGuid = cyclopsGuid;
            Room = room;
            NodeIndex = nodeIndex;
        }

        public override string ToString()
        {
            return "[独眼巨人号火力数据"
                + " 火Guid: " + FireGuid
                + " 独眼巨人号Guid: " + CyclopsGuid
                + " 房间: " + Room
                + " 火节点索引: " + NodeIndex
                + "]";
        }
    }
}
