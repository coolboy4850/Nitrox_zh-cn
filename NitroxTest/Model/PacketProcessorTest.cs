﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NitroxClient;
using NitroxClient.Communication.Packets.Processors.Abstract;
using NitroxClient.MonoBehaviours;
using NitroxModel.Core;
using NitroxModel.Packets;
using NitroxModel.Packets.Processors.Abstract;
using NitroxServer.Communication.Packets;
using NitroxServer.Communication.Packets.Processors.Abstract;
using NitroxServer.GameLogic;
using NitroxServer.Serialization.World;
using NitroxServer;
using NitroxServer.Communication.Packets.Processors;

namespace NitroxTest.Model
{
    [TestClass]
    public class PacketProcessorTest
    {
        [TestMethod]
        public void ClientPacketProcessorSanity()
        {
            typeof(Multiplayer).Assembly.GetTypes()
                .Where(p => typeof(PacketProcessor).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract)
                .ToList()
                .ForEach(processor =>
                {
                    // Make sure that each packetprocessor is derived from the ClientPacketProcessor class,
                    //  so that it's packet-type can be determined.
                    Assert.IsTrue(processor.BaseType.IsGenericType, $"{processor} 不从泛型类型派生!");
                    Assert.IsTrue(processor.BaseType.GetGenericTypeDefinition() == typeof(ClientPacketProcessor<>), $"{processor} 不从客户端数据包处理器派生!");

                    // Check constructor availability:
                    int numCtors = processor.GetConstructors().Length;
                    Assert.IsTrue(numCtors == 1, $"{processor} 应该正好有一个构造函数！ (有 {numCtors})");
                });
        }

        [TestMethod]
        public void RuntimeDetectsAllClientPacketProcessors()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();
            ClientAutoFacRegistrar clientDependencyRegistrar = new ClientAutoFacRegistrar();
            clientDependencyRegistrar.RegisterDependencies(containerBuilder);
            IContainer clientDependencyContainer = containerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            // Check if every PacketProcessor has been detected:
            typeof(Multiplayer).Assembly.GetTypes()
                .Where(p => typeof(PacketProcessor).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract)
                .ToList()
                .ForEach(processor =>
                    Assert.IsTrue(clientDependencyContainer.Resolve(processor.BaseType) != null,
                        $"{processor} 运行时代码未发现!")
                );
        }

        [TestMethod]
        public void ServerPacketProcessorSanity()
        {
            typeof(PacketHandler).Assembly.GetTypes()
                .Where(p => typeof(PacketProcessor).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract)
                .ToList()
                .ForEach(processor =>
                {
                    // Make sure that each packetprocessor is derived from the ClientPacketProcessor class,
                    //  so that it's packet-type can be determined.
                    Assert.IsTrue(processor.BaseType.IsGenericType, $"{processor} 不从泛型类型派生!");
                    Type baseGenericType = processor.BaseType.GetGenericTypeDefinition();
                    Assert.IsTrue(baseGenericType == typeof(AuthenticatedPacketProcessor<>) || baseGenericType == typeof(UnauthenticatedPacketProcessor<>), $"{processor} 不从（非）验证数据包处理器派生!");

                    // Check constructor availability:
                    int numCtors = processor.GetConstructors().Length;
                    Assert.IsTrue(numCtors == 1, $"{processor} 应该正好有一个构造函数！ (有 {numCtors})");

                    // Unable to check parameters, these are defined in PacketHandler.ctor
                });
        }

        [TestMethod]
        public void SameAmountOfServerPacketProcessors()
        {
            IEnumerable<Type> processors = typeof(PacketHandler).Assembly.GetTypes()
                .Where(p => typeof(PacketProcessor).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract);
            World world = new World();
            ContainerBuilder serverContainerBuilder = new ContainerBuilder();
            ServerAutoFacRegistrar serverDependencyRegistrar = new ServerAutoFacRegistrar();
            serverDependencyRegistrar.RegisterDependencies(serverContainerBuilder);
            IContainer serverDependencyContainer = serverContainerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            List<Type> packetTypes = typeof(DefaultServerPacketProcessor).Assembly.GetTypes()
                .Where(p => typeof(PacketProcessor).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract)
                .ToList();

            int both = packetTypes.Count;
            Assert.AreEqual(processors.Count(), both,
                "运行时代码未发现所有（非）身份验证数据包处理器 " +
                $"(验证 + 非验证: {both} 由于 {processors.Count()}). " + // this is a small patch to keep this alive a little longer until its put out of its misery
                "可能运行时匹配代码太严格，或者处理器不是从客户端数据包处理器派生的 " +
                "(因此不会被发现)");
        }

        [TestMethod]
        public void RuntimeDetectsAllServerPacketProcessors()
        {
            World world = new World();

            ContainerBuilder containerBuilder = new ContainerBuilder();
            ServerAutoFacRegistrar serverDependencyRegistrar = new ServerAutoFacRegistrar();
            serverDependencyRegistrar.RegisterDependencies(containerBuilder);
            IContainer serverDependencyContainer = containerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            // Check if every PacketProcessor has been detected:
            typeof(DefaultServerPacketProcessor).Assembly.GetTypes()
                .Where(p => typeof(PacketProcessor).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract)
                .ToList()
                .ForEach(processor =>
                    Assert.IsTrue(serverDependencyContainer.Resolve(processor.BaseType) != null,
                        $"{processor} 运行时代码未发现!")
                );
        }

        [TestMethod]
        public void AllPacketsAreHandled()
        {
            World world = new World();
            ContainerBuilder serverContainerBuilder = new ContainerBuilder();
            ServerAutoFacRegistrar serverDependencyRegistrar = new ServerAutoFacRegistrar();
            serverDependencyRegistrar.RegisterDependencies(serverContainerBuilder);
            IContainer serverDependencyContainer = serverContainerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            List<Type> packetTypes = typeof(DefaultServerPacketProcessor).Assembly.GetTypes()
                .Where(p => typeof(PacketProcessor).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract)
                .ToList();

            ContainerBuilder containerBuilder = new ContainerBuilder();
            ClientAutoFacRegistrar clientDependencyRegistrar = new ClientAutoFacRegistrar();
            clientDependencyRegistrar.RegisterDependencies(containerBuilder);
            IContainer clientDependencyContainer = containerBuilder.Build(ContainerBuildOptions.IgnoreStartableComponents);

            typeof(Packet).Assembly.GetTypes()
                .Where(p => typeof(Packet).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract)
                .ToList()
                .ForEach(packet =>
                {
                    Type clientPacketProcessorType = typeof(ClientPacketProcessor<>);
                    Type clientProcessorType = clientPacketProcessorType.MakeGenericType(packet);

                    Console.WriteLine("正在检查数据包的处理程序 {0}...", packet);
                    Assert.IsTrue(packetTypes.Contains(packet) || clientDependencyContainer.Resolve(clientProcessorType) != null,
                        $"运行时未检测到的处理程序 {packet}!");
                }
                );
        }
    }
}
